SUMMARY="Reads and writes MNG images."
DESCRIPTION="
LibMNG reads and writes MNG format image files, a PNG-like image format \
supporting multiple image, animation and transparent JPEG.
"
HOMEPAGE="http://www.libmng.com" 
COPYRIGHT="2000-2007 Gerard Juyn"
LICENSE="LIBMNG"
SRC_URI="http://sourceforge.net/projects/libmng/files/libmng-devel/$portVersion/libmng-$portVersion.tar.xz"
CHECKSUM_SHA256="278c04c781e5a679c91df006fee7e71233e6f15557aef513a19fe49cd650bc50"
REVISION="1"

ARCHITECTURES="x86_gcc2 x86 x86_64"
SECONDARY_ARCHITECTURES="x86_gcc2 x86"

PATCHES="libmng-$portVersion.patchset"

PROVIDES="
	libmng$secondaryArchSuffix = $portVersion compat >= 2.0
	lib:libmng$secondaryArchSuffix = 2.0.2 compat >= 2
	"
REQUIRES="
	haiku$secondaryArchSuffix
	lib:libjpeg$secondaryArchSuffix
	lib:libz$secondaryArchSuffix
	"
BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel
	devel:libjpeg$secondaryArchSuffix
	devel:libz$secondaryArchSuffix
	"
BUILD_PREREQUIRES="
	cmd:aclocal
	cmd:autoconf
	cmd:automake
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:libtoolize
	cmd:make
	"

BUILD()
{
	autoreconf -fi
	runConfigure ./configure
	make $jobArgs
}

INSTALL()
{
	make install

	prepareInstalledDevelLibs libmng
	fixPkgconfig

	# devel package
	packageEntries devel \
		$developDir \
		$manDir/man3 $manDir/man5
}

TEST()
{
	make check
}

# ----- devel package -------------------------------------------------------

PROVIDES_devel="
	libmng${secondaryArchSuffix}_devel = $portVersion compat >= 2.0
	devel:libmng$secondaryArchSuffix = 2.0.2 compat >= 2
	"
REQUIRES_devel="
	libmng$secondaryArchSuffix == $portVersion base
	"
